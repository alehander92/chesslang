import game

type
  SimpleMove* = ref object
    `from`*:        Location
    target*:        Location
    attack*:        Location
    changeFigure*:  ChangeFigure
    captures*:      bool

  NewGameColor* = ref object
    color*: Color

  NewGame* = ref object
    when defined(js):
      link*: cstring
    else:
      link*:  string
    color*: Color

  InviteAccepted* = ref object
    when defined(js):
      link*: cstring
    else:
      link*: string

  NewMove* = ref object
    when defined(js):
      name*: cstring
    else:
      name*: string
    move*: SimpleMove

  ServerMessage*[T] = object
    when defined(js):
      message*: cstring
    else:
      message*: string
    value*: T


proc toMove*(move: SimpleMove, game: Game): Move =
  Move(
    `from`: move.`from`,
    target: move.target,
    attack: move.attack,
    changeFigure: move.changeFigure,
    captures: move.captures,
    figure: game.board[move.`from`.line][move.`from`.column])


proc toSimpleMove*(move: Move): SimpleMove =
  SimpleMove(
    `from`: move.`from`,
    target: move.target,
    attack: move.attack,
    changeFigure: move.changeFigure,
    captures: move.captures)
