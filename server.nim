import strformat, sequtils, strutils, tables, sets, json, terminal, macros
import websocket/shared, websocket/server, asynchttpserver, asyncdispatch, asyncnet, uri, cookies, httpcore
import game, types


var s = newAsyncHttpServer()


proc styled*[T](f: ForegroundColor, s: T) =
  styledEcho f, s, resetStyle

proc success*[T](s: T) =
  styled fgGreen, s

proc warn*[T](s: T) =
  styled fgYellow, s

proc fail*[T](s: T) =
  styled fgRed, s

proc blue*[T](s: T) =
  styled fgBlue, s


# temp

type
  DB* = ref object
    players*:       Table[string, Player]
    links*:         seq[Link]

  Player* = ref object
    name*:          string
    games*:         seq[Game]
    activeGame*:    Game

  Link* = ref object
    waiting*:       bool
    game*:          Game


proc respondInvite(req: Request, link: int) {.async, gcsafe.}

let db = DB(players: initTable[string, Player](), links: @[])

var requests = initTable[string, Request]()

# db methods


proc playerWithName(db: DB, name: string): Player =
  var players = db.players
  if not db.players.hasKey(name):
    db.players[name] = Player(name: name, games: @[], activeGame: nil)
  db.players[name]


proc genLink(db: DB, game: Game): string =
  db.links.add(Link(waiting: true, game: game))
  $(db.links.len - 1)

proc send[T](req: Request, message: string, value: T) {.async, gcsafe.} =
  let mail = ServerMessage[T](message: message, value: value)
  discard req.client.sendText(pretty(%mail), false)


proc receive(req: Request, data: string) {.async, gcsafe.} =
  if data.len == 0 or data[0] != '{':
    return
  try:
    let obj = data.parseJson
    let msg = obj{"message"}.getStr
    case msg:
    of "new-game":
      let value = obj{"value"}.to(NewGameColor)
      let player = db.playerWithName(req.headers["sec-websocket-key"])
      # TODO find ruleset in db
      let ruleset = defaultRuleset
      let game = newGame(ruleset)
      game.names[Color.White] = player.name
      let link = db.genLink(game)
      game.onNewMove()
      waitFor req.send("new-game", NewGame(link: link, color: value.color))
    of "new-move":
      let value = obj{"value"}.to(NewMove)
      let name = req.headers["sec-websocket-key"]
      if db.players.hasKey(name):
        let player = db.players[name]
        echo name
        let color = if name == player.activeGame.names[Color.White]: Color.White else: Color.Black
        if player.activeGame.turn == color:
          player.activeGame.onPiece(value.move.`from`)
          player.activeGame.onDropPiece(value.move.attack)
          # player.activeGame.turn = ~player.activeGame.turn
          let otherName = player.activeGame.names[player.activeGame.turn]
          player.activeGame.onNewMove()
          blue "WHITE: " & name
          blue "BLACK: " & otherName
          blue "REQUESTS:"
          for label, req in requests:
            blue "  " & label
          waitFor requests[otherName].send("new-move", NewMove(name: otherName, move: value.move))
    of "invite-accepted":
      let value = obj{"value"}.to(InviteAccepted)
      let link = value.link.parseInt
      if db.links.len <= link:
        return
      waitFor req.respondInvite(link)
    else:
      discard
  except:
    fail getCurrentExceptionMsg()


proc respondInvite(req: Request, link: int) {.async, gcsafe.} =
  let l = db.links[link]
  let player = db.playerWithName(req.headers["sec-websocket-key"])
  let other = db.players[l.game.names[Color.White]]
  let game = l.game
  player.activeGame = game
  other.activeGame = game
  game.names[Color.Black] = player.name
  blue &"PLAYERS: {player.name} {other.name}"
  player.games.add(game)


macro render(req: untyped, file: untyped, args: untyped): untyped =
  result = quote:
    echo "<= 200 file: " & `file`
    `req`.respond(Http200, readFile(`file`) % `args`)
  echo result.repr

macro render(req: untyped, file: untyped): untyped =
  result = quote:
    echo "<= 200 file: " & `file`
    `req`.respond(Http200, readFile(`file`))
  echo result.repr
    
proc cb(req: Request) {.async, gcsafe.} =
  
  let (success, error) = await(verifyWebsocketRequest(req, ""))
  if not success:
    if req.reqMethod == HttpGet:
      echo &"=> GET {req.url.path}"
      if req.url.path.startsWith("/invite/"):
        try:
          let link = req.url.path.rsplit("/", 1)[1].parseInt
          await req.render("client/index.html", ["link", $link, "color", "1"])
          await req.respond(Http200, readFile("client/index.html") % ["link", $link, "color", "1"])
        except:
          fail getCurrentExceptionMsg()
      elif req.url.path == "/":
        await req.respond(Http200, readFile("client/index.html") % ["link", "", "color", "0"])
      elif req.url.path.startswith("/styles/") and 
           req.url.path.endsWith(".css") or 
           req.url.path.startswith("/webfonts/") and 
           (req.url.path.endsWith(".woff") or req.url.path.endsWith(".woff2")):
        await req.respond(Http200, readFile(&"client{req.url.path}"))
      elif req.url.path == "/logic.js":
        await req.respond(Http200, readFile(&"client/logic.js"))
      elif req.url.path.endswith("/logic.js.map"):
        await req.respond(Http200, readFile(&"client/logic.js.map"))
      else:
        await req.respond(Http404, "?")
    else:
      fail "WS negotiation failed: " & error
      await req.respond(Http400, "Websocket negotiation failed: " & error)
    req.client.close()
  else:
    success "New websocket customer arrived!"
    requests[req.headers["sec-websocket-key"]] = req
    while true:
      try:
        var f = await req.client.readData(false)
        echo $f.data
        
        if f.opcode == Opcode.Text:
          waitFor req.receive($f.data)
          # req.send("thanks for the data", 0)
        else:
          waitFor req.client.sendBinary(f.data, false)
      
      except:
        fail getCurrentExceptionMsg()
        break
    
    req.client.close()
    warn".. socket went away"


asyncCheck s.serve(Port 8000, cb)
runForever()
