import karax / [karax, vdom, karaxdsl], async, jsffi, strformat, strutils, sequtils, tables
import .. / game, .. / types

type
  WebSocket* = ref object
    onopen*: proc(response: js)
    onclose*: proc(response: js)
    onmessage*: proc(response: js)
    onerror*: proc(response: js)
    send*: proc(s: cstring)

  Response* = ref object
    message*: cstring
    value*: js

  State* = ref object
    game*:                Game
    color*:               Color
    link*:                cstring
    attackedBoard*:       array[1 .. MAX_LINE, array[1 .. MAX_COLUMN, bool]]
    moving*:              bool

var link {.importc.}: cstring
var color {.importc.}: Color


proc fa*(typ: string, kl: string = ""): VNode =
  result = buildHtml(italic(class="fa fa-$1 $2" % [$typ, kl]))


template j*(s: string): cstring =
  cstring(s)

proc newWebSocket(uri: cstring): WebSocket {.importcpp: "new WebSocket(#)".}
proc toJson[T](t: T): cstring {.importcpp: "JSON.stringify(#)".}
proc parseJson[T](value: js): T {.importcpp: "JSON.parse(#)".}

let ws = newWebSocket(j"ws://localhost:8000")
let state = if link.len == 0:
    State(game: nil, link: j"")
  else:
    # TODO receive ruleset
    let ruleset = defaultRuleset
    State(game: newGame(ruleset), link: link, color: color)


proc send[T](message: string, t: T) =
  ws.send(js{message: j(message), value: t}.toJson)


proc startNewGame(ruleset: Ruleset) =
  state.game = newGame(ruleset)
  state.color = Color.White
  send "new-game", js{color: Color.White}
  state.game.onNewMove()




# HANDLERS


proc onNewGame(value: NewGame) =
  state.color = value.color
  state.link = j(&"/invite/{value.link}")
  redraw()


proc onNewMove(value: NewMove) =
  # manually upgrade new board, faster
  let move = value.move
  let figure = state.game.board[move.`from`.line][move.`from`.column]
  figure.line = move.target.line
  figure.column = move.target.column
  let attacked = state.game.board[move.attack.line][move.attack.column]
  if not attacked.isNil and attacked.lives == 1 and move.target != move.attack:
    state.game.board[move.attack.line][move.attack.column] = nil
  state.game.board[move.`from`.line][move.`from`.column] = nil
  state.game.board[move.target.line][move.target.column] = figure
  state.game.turn = ~state.game.turn
  redraw()
  state.game.onNewMove()

# UI

proc onPiece(state: State, location: Location) =
  if not state.moving:
    state.game.onPiece(location)
    if state.game.currentPiece.isNil:
      return
    state.moving = true
    for move in state.game.possibleMoves[location.line][location.column]:
      state.attackedBoard[move.attack.line][move.attack.column] = true
  else:
    state.moving = false
    if state.game.currentPiece.isNil:
      return
    let newMove = state.game.currentPiece.location != location
    if newMove:
      state.game.onDropPiece(location)
    for line in 1 .. MAX_LINE:
      for column in 1 .. MAX_COLUMN:
        state.attackedBoard[line][column] = false
    if newMove:
      send "new-move", NewMove(name: j"", move: state.game.moves[state.color][^1].toSimpleMove)


proc newFigureView: VNode =
  result = buildHtml(tdiv(class="new-figure"))


proc newGameView: VNode =
  # TODO: ruleset menu
  let ruleset = defaultRuleset
  result = buildHtml(tdiv(class="new-game", onclick=proc(ev: Event, v: VNode) = startNewGame(ruleset))):
    text("new game")


proc listFiguresView: VNode =
  buildHtml(tdiv(class="figures"))


proc listGamesView: VNode =
  buildHtml(tdiv(class="games"))


proc figureView(state: State, figure: Figure): VNode =
  buildHtml(tdiv(class = &"board-figure color-{($figure.color).toLowerAscii}")):
    fa(&"{figure.game.ruleset.rules[figure.kind].faSymbol}")


proc cellView(state: State, color: Color, line: int, column: int): VNode =
  let attackedClass = if state.attackedBoard[line][column]: "attacked" else: ""
  buildHtml(tdiv(
      class = &"board-cell background-{($color).toLowerAscii} column-{column} cell-{line}-{column} {attackedClass}",
      onclick = proc(ev: Event, v: VNode) = state.onPiece(Location(line: line, column: column)))):
    let figure = state.game.board[line][column]
    if figure.isNil:
      text("")
    else:
      figureView(state, figure)


proc rule*(location: cstring): Rule {.exportc.} =
  let cell = ($location).cell
  state.game.ruleset.rules[state.game.board[cell.line][cell.column].kind]


proc boardView(state: State): VNode =
  var color = ((MAX_LINE + MAX_COLUMN) mod 2).Color
  var first: int
  var last: int
  if state.color == Color.White:
    first = MAX_LINE
    last = 1
  else:
    first = 1
    last = MAX_LINE

  buildHtml(tdiv(class="board")):
    var line = first
    while first < last and line <= last  or first > last and line >= last:
      tdiv(class = &"board-line line-{line}"):
        for column in 1 .. MAX_COLUMN:
          cellView(state, color, line, column)
          color = ~color
      if first < last:
        line += 1
      else:
        line -= 1
      color = ~color


proc gameView(state: State): VNode =
  result = buildHtml(tdiv(class="game")):
    boardView(state)


proc linkView(state: State): VNode =
  let link = &"http://localhost:8000{state.link}"
  buildHtml(tdiv(class="link")):
    a(href=link):
      text(&"add player {link}")

proc main: VNode =
  result = buildHtml(tdiv(class="menu")):
    if state.game.isNil:
      newFigureView()
      newGameView()
      listFiguresView()
      listGamesView()
    else:
      if link.len == 0:
        linkView(state)
      gameView(state)


setRenderer main, "main", proc = discard
redraw()

ws.onopen = proc(response: js) = 
  ws.send(j"HELLO")
  if color == Color.Black:
    send "invite-accepted", InviteAccepted(link: link)


ws.onmessage = proc(response: js) =
  let data = parseJson[Response](response.data)
  kout data
  case $data.message:
  of "new-game": onNewGame(cast[NewGame](data.value))
  of "new-move": onNewMove(cast[NewMove](data.value))
  else: discard

