# chess: simpler


# Queen:
#   all: N

# Rook:
#   horizontal: N
#   vertical: N

# Knight:
#   horizontal: 2 vertical: 1
#   vertical: 2 horizontal: 1

# Bishop:
#   diagonal: N

# King:
#   all: 1

# Pawn:
#   @ line == 2:
#   	up: 1..2
#   @ line in 3..6:
#     up: 1
#   @ line == 7:
#     up: 1 changeFigure: all
#   @ rivalFigure on upLeft#1:
#     upLeft: 1
#   @ rivalFigure on upRight#1:
#     upRight: 1
#   @ line == 5 and rivalPawn on left#1 as pawn and lastMove.figure == pawn and lastMove.`from`.line == 2:
#     left: 1
#   @ line == 5 and rivalPawn of right#1 as pawn and lastMove.figure == pawn and lastMove.`from`.line == 2:
#     right: 1

# Bird:
#   all: N
#   Jump: true

# Cat:
#   all: 1..2
#   Lives: 3

# Chameleon:
#   horizontal: N changeFigure: all
#   vertical: N changeFigure: all

import strformat, strutils, sequtils, tables, json, macros, sugar

const
  MAX_LINE* = 8
  MAX_COLUMN* = 8
  VARIABLE_N* = (MAX_LINE + MAX_COLUMN) * 4


type
  Move* = ref object
    `from`*:        Location
    target*:        Location
    attack*:        Location
    figure*:        Figure
    changeFigure*:  ChangeFigure
    captures*:      bool

  Location* = ref object
    line*:          int
    column*:        int

  Finish* {.pure.} = enum Checkmate, Stalemate

  ChangeFigure* {.pure.} = enum None, All, Random, Pawn

  Color* {.pure.} = enum White, Black

  Direction* = enum
    Right,
    Left,
    Up,
    Down,
    UpRight,
    DownRight,
    UpLeft,
    DownLeft,
    All,
    Vertical,
    Horizontal,
    Diagonal

  Line* = distinct int

  Column* = distinct int

  Figure* = ref object
    line*:          int
    column*:        int
    color*:         Color
    kind*:          string
    lives*:         int
    jump*:          bool
    changeFigure*:  ChangeFigure
    game*:          Game

  Game* = ref object
    board*:                 array[1 .. MAX_LINE, array[1 .. MAX_COLUMN, Figure]]
    moves*:                 array[Color, seq[Move]]
    turn*:                  Color
    possibleMoves*:         array[1 .. MAX_LINE, array[1 .. MAX_COLUMN, seq[Move]]]
    possibleAnswers*:       array[1 .. MAX_LINE, array[1 .. MAX_COLUMN, seq[Move]]]
    currentPiece*:          Figure
    kingLocations*:         array[Color, Location]
    inCheckers*:            array[Color, bool]
    possibleMovesCount*:    int
    possibleMovesCached*:   bool
    names*:                 array[Color, string]
    ruleset*:               Ruleset

  Rule* = ref object
    figureKind*:             string
    unicodeSymbols*:         array[Color, string]
    faSymbol*:               string # color it
    name*:                   string
    builtin*:                bool
    start*:                  seq[Location]
    moves*:                  seq[RuleMove]
    jump*:                   bool
    lives*:                  int
  
  RuleMove* = ref object
    subMoves*:               seq[SubMove]
    changeFigure*:           bool

  SubMove* = ref object
    direction*:              Direction
    cells*:                  Cells

  Ruleset* = ref object
    rules*:                  Table[string, Rule]
    name*:                   string
    repetitionsDraw*:        int # -1 false
    noCaptureDraw*:          int # -1 false

  CellsKind* = enum CInt, CVariable, CRange

  Cells* = ref object
    case kind*: CellsKind:
    of CInt:
      i*: int
    of CVariable:
      discard
    of CRange:
      r*: Slice[int]

proc cell*(source: string): Location =
  let line = ($source[1]).parseInt
  let column = (source[0].ord - 'a'.ord) + 1
  Location(line: line, column: column)


proc `%`*(cells: Cells): JsonNode =
  # easier
  case cells.kind:
  of CInt:
    %(cells.i)
  of CVariable:
    %"N"
  of CRange:
    %($cells.r)


proc `%`*[K, V](t: Table[K, V]): JsonNode =
  result = newJObject()
  for key, value in t:
    result[key] = %value


proc `$`*(ruleset: Ruleset): string =
  pretty(%ruleset)


proc offset(line: string): int =
  var count = 0
  for c in line:
    if c == ' ':
      count += 1
    else:
      break
  result = count div 2


proc parseOption(line: string, t: type string): string =
  line.rsplit(" ", 1)[1]


proc parseOption(line: string, t: type int): int =
  line.parseOption(string).parseInt


proc genSubMove(direction: string, rawCells: string): SubMove =
  var cells: Cells
  if rawCells == "N":
    cells = Cells(kind: CVariable)
  elif ".." in rawCells:
    let tokens = rawCells.rsplit("..", 1)
    cells = Cells(kind: CRange, r: tokens[0].parseInt..tokens[1].parseInt)
  else:
    cells = Cells(kind: CInt, i: rawCells.parseInt)
  let directions = {
    "right": Right,
    "left": Left,
    "up": Up,
    "down": Down,
    "upright": UpRight,
    "upleft": UpLeft,
    "downright": DownRight,
    "downleft": DownLeft,
    "horizontal": Horizontal,
    "vertical": Vertical,
    "diagonal": Diagonal,
    "all": All
  }.toTable()
  SubMove(direction: directions[direction], cells: cells)


proc genRuleset*(source: string): Ruleset =
  result = Ruleset(rules: initTable[string, Rule]())
  let lines = source.splitLines()
  result.name = lines[0].parseOption(string) # [ "name", "default" ]#
  result.repetitionsDraw = lines[1].parseOption(int)
  result.noCaptureDraw = lines[2].parseOption(int)
  var z = 3
  var line = ""
  while z < lines.len:
    if lines[z].len < 2:
      break
    let name = lines[z][0 .. ^2]
    echo name
    z += 1
    line = lines[z]
    var offset = line.offset
    let rule = Rule(figureKind: name, name: name, start: @[], moves: @[], lives: 1, builtin: false)
    echo "  ", line, " ", offset
    while offset == 1:
      var tokens = line[2 .. ^1].split(" ")
      let key = tokens[0][0 .. ^2]
      var afterMove = false
      case key:
      of "unicodeSymbols":
        rule.unicodeSymbols = [tokens[1], tokens[2]]
      of "faSymbol":
        rule.faSymbol = tokens[1]
      of "builtin":
        rule.builtin = tokens[1] == "true"
      of "start":
        rule.start = tokens[1 .. ^1].mapIt(it.cell)
      of "lives":
        rule.lives = tokens[1].parseInt
      of "jump":
        rule.jump = tokens[1] == "true"
      of "moves":
        z += 1
        line = lines[z]
        offset = line.offset
        afterMove = true
        while offset == 2:
          tokens = line[4 .. ^1].split(" ")
          rule.moves.add(RuleMove(subMoves: @[]))
          # op arg op arg
          # comb explosion, genMove level
          for y in 0 ..< tokens.len div 2:
            if tokens[y * 2] == "changeFigure:" and tokens[y * 2 + 1] == "true":
              rule.moves[^1].changeFigure = true
            else:
              # echo tokens
              # echo tokens[y * 2][0 .. ^2]
              # echo tokens[y * 2 + 1]
              rule.moves[^1].submoves.add(genSubMove(tokens[y * 2][0 .. ^2], tokens[y * 2 + 1]))
          z += 1
          if z >= lines.len:
            break
          line = lines[z]
          offset = line.offset
      else:
        discard
      result.rules[name] = rule
      # if after move, we are already on figure:
      if not afterMove:
        z += 1
        if z >= lines.len:
          break
        line = lines[z]
        offset = line.offset


let kingRule = """
king:
  unicodeSymbols: ♔ ♚
  faSymbol: chess-king
  builtin: true
  start: e1
  moves:
    all: 1
"""

let queenRule = """
queen:
  unicodeSymbols: ♕ ♛
  faSymbol: chess-queen
  builtin: true
  start: d1
  moves:
    all: N
"""

let rookRule = """
rook:
  unicodeSymbols: ♖ ♜
  faSymbol: chess-rook
  builtin: true
  start: a1 h1
  moves:
    horizontal: N
    vertical: N
"""

let knightRule = """
knight:
  unicodeSymbols: ♘ ♞
  faSymbol: chess-knight
  builtin: true
  start: b1 g1
  moves:
    horizontal: 2 vertical: 1
    vertical: 2 horizontal: 1
"""

let bishopRule = """
bishop:
  unicodeSymbols: ♗ ♝
  faSymbol: chess-bishop
  builtin: true
  start: c1 f1
  moves:
    diagonal: N
"""

let defaultRuleset* = genRuleset(&"""
name: default
repetitionsDraw: 3
noCaptureDraw: 50
{kingRule}{queenRule}{rookRule}{knightRule}{bishopRule}
""") # bishops !

let pawnRule = Rule(
      figureKind: "pawn",
      unicodeSymbols: ["♗", "♟"],
      faSymbol: "chess-pawn",
      builtin: true,
      start: @["a2", "b2", "c2", "d2", "e2", "f2", "g2", "h2"].mapIt(it.cell),
      moves: @[], # builtin
      jump: false,
      lives: 1)

defaultRuleset.rules["pawn"] = pawnRule

echo defaultRuleset

proc `$`*(location: Location): string =
  let letter = " abcdefgh"[location.column]
  &"{letter}{location.line}"


proc `$`*(move: Move): string =
  &"{move.`from`} {move.attack}"


proc textFigure*(figure: Figure): string =
  if figure.isNil:
    "_"
  else:
    figure.game.ruleset.rules[figure.kind].unicodeSymbols[figure.color]


proc textBoard*(game: Game): string =
  result = ""
  for line in game.board:
    result.add(line.mapIt(it.textFigure).join(" ") & "\n")


proc dumpBoard*(game: Game) =
  echo game.textBoard

  
template debug*(s: untyped): untyped =
  # no simulate
  if not simulate:
    echo `s`


template extendResult(moves: seq[Move]) =
  result = result.concat(moves)
  let a {.inject.} = moves.mapIt($it).join(",")
  debug &"    {a}"


proc `~`*(color: Color): Color =
  (1 - color.int).Color


proc `==`*(a: Location, b: Location): bool =
  a.line == b.line and a.column == b.column


proc lastMove(game: Game, color: Color): Move =
  if game.moves[color].len > 0:
    game.moves[color][^1]
  else:
    nil

proc location*(figure: Figure): Location =
  Location(line: figure.line, column: figure.column)


proc rivalWith(figure: Figure, color: Color): bool =
  not figure.isNil and figure.color != color


proc relative(line: int, color: Color): int =
  if color == Color.White:
    line
  else:
    MAX_LINE + 1 - line


proc relative(direction: Direction, color: Color): Direction =
  if color == Color.White:
    direction
  else:
    case direction:
      of Right: Left
      of Up: Down
      of Left: Right
      of Down: Up
      of UpRight: DownLeft
      of DownRight: UpLeft
      of UpLeft: DownRight
      of DownLeft: UpRight
      of Horizontal: Horizontal
      of Vertical: Vertical
      of Diagonal: Diagonal
      of All: All



proc on(game: Game, location: Location): Figure =
  if location.line in 1..MAX_LINE and location.column in 1..MAX_COLUMN:
    game.board[location.line][location.column]
  else:
    nil


proc `+`(z: int, line: Line): int =
  z + line.int


proc `+`(z: int, column: Column): int =
  z + column.int


proc `-`(z: int, line: Line): int =
  z - line.int


proc `-`(z: int, column: Column): int =
  z - column.int


proc `+`(location: Location, line: Line): Location =
  Location(line: location.line + line, column: location.column)


proc `+`(location: Location, column: Column): Location =
  Location(line: location.line, column: location.column + column)


proc `-`(location: Location, line: Line): Location =
  Location(line: location.line - line, column: location.column)


proc `-`(location: Location, column: Column): Location =
  Location(line: location.line, column: location.column - column)


proc genLocation(location: Location, direction: Direction, z: int): Location =
  case direction:
  of Right: location + z.Column
  of Up:    location + z.Line
  of Left:  location - z.Column
  of Down: location - z.Line
  of UpRight: location + z.Line + z.Column
  of DownRight: location - z.Line + z.Column
  of UpLeft: location + z.Line - z.Column
  of DownLeft: location - z.Line - z.Column
  else: location


proc on(figure: Figure, direction: Direction, z: int): Figure =
  figure.game.on(figure.location.genLocation(direction.relative(figure.color), z))
  

proc genMove(figure: Figure, direction: Direction, z: int, simulate: bool, capture: bool = true): Move =
  let attack = figure.on(direction, z)
  let attackLocation = figure.location.genLocation(direction.relative(figure.color), z)
  # debug &"{figure.location} {direction}{z} {attackLocation}"
  if attackLocation.line notin 1 .. MAX_LINE or attackLocation.column notin 1 .. MAX_COLUMN:
    return nil

  if not attack.isNil:
    if attack.color == figure.color:
      return nil
    elif not capture:
      return nil

  result = Move(
    `from`: figure.location,
    attack: attackLocation,
    target: attackLocation,
    captures: false,
    changeFigure: figure.changeFigure)
  if not attack.isNil:
    result.captures = true
    if attack.lives > 1:
      result.target = result.`from`


const
  NORMAL_DIRECTIONS = {Right, Up, Left, Down, UpRight, UpLeft, DownRight, DownLeft}
  HORIZONTAL_DIRECTIONS = @[Right, Left]
  VERTICAL_DIRECTIONS = @[Up, Down]
  DIAGONAL_DIRECTIONS = @[UpRight, UpLeft, DownRight, DownLeft]


proc expandDirection(direction: Direction): seq[Direction] =
  case direction:
  of Horizontal:
    result = HORIZONTAL_DIRECTIONS
  of Vertical:
    result = VERTICAL_DIRECTIONS
  of Diagonal:
    result = DIAGONAL_DIRECTIONS
  of All:
    result = HORIZONTAL_DIRECTIONS.concat(VERTICAL_DIRECTIONS).concat(DIAGONAL_DIRECTIONS)
  else:
    result = @[direction]


proc genMoves(figure: Figure, direction: Direction, z: int, simulate: bool, capture: bool = true): seq[Move] =
  if direction in NORMAL_DIRECTIONS:
    let move = figure.genMove(direction, z, capture)
    if move.isNil:
      result = @[]
    else:
      result = @[move]
  else:
    result = @[]
    let directions = expandDirection(direction)
    for dir in directions:
      result = result.concat(figure.genMoves(dir, z, capture))


proc genMoves(figure: Figure, direction: Direction, r: Slice[int], simulate: bool, capture: bool = true): seq[Move] =
  result = @[]
  let directions = expandDirection(direction)
  for dir in directions:
    debug &"    {dir}:"
    for z in r:
      debug &"      {z}:"
      let move = figure.genMove(dir, z, simulate, capture)
      debug $move.isNil
      if not move.isNil:
        result.add(move)
        if move.captures:
          break
      if z > max(MAX_LINE, MAX_COLUMN) - r.a:
        break


proc possiblePawnMoves(figure: Figure, simulate: bool = false): seq[Move] =
  result = @[]
  let line = figure.line.relative(figure.color)

  proc inAnPasan(rival: Figure): bool =
    rival.rivalWith(figure.color) and
      not figure.game.lastMove(rival.color).isNil and
      figure.game.lastMove(rival.color).figure == rival and
      figure.game.lastMove(rival.color).`from`.line.relative(rival.color) == 2


  debug &"GENERATE {line} {figure.column}:"
  if line == 2:
    debug "  2 Up 1..2:"
    extendResult figure.genMoves(Up, 1..2, simulate, capture=false)
  elif line in 3..6:
    debug &"  3..6 Up 1:"
    extendResult figure.genMoves(Up, 1, simulate, capture=false)
  elif line == 7:
    debug "  7 Up 1:"
    let moves = figure.genMoves(Up, 1, simulate, capture=false)
    if moves.len == 1:
      let move = moves[0]
      move.changeFigure = ChangeFigure.All
      result.add(move)
      debug &"    {move}"
  
  if figure.on(UpLeft, 1).rivalWith(figure.color):
    debug &"  UpLeft 1:"
    extendResult figure.genMoves(UpLeft, 1, simulate)
  if figure.on(UpRight, 1).rivalWith(figure.color):
    debug &"  UpRight 1:"
    extendResult figure.genMoves(UpRight, 1, simulate)
  
  if line == 5:
    let onLeft = figure.on(Left, 1)
    if inAnPasan(onLeft):
      debug &"  AnPasan 5 Left 1:"
      extendResult figure.genMoves(Left, 1, simulate)
    let onRight = figure.on(Right, 1)
    if inAnPasan(onRight):
      debug &"  AnPasan 5 Right 1:"
      extendResult figure.genMoves(Right, 1, simulate)


proc possibleRuleMoves(figure: Figure, simulate: bool = false): seq[Move] =
  result = @[]
  let line = figure.line.relative(figure.color)

  debug &"GENERATE {line} {figure.column}:"
  debug &"{figure.kind}"
  for move in figure.game.ruleset.rules[figure.kind].moves:
    for subMove in move.submoves:
      case subMove.cells.kind:
      of CInt:
        debug &"  SUB {subMove.direction} {subMove.cells.i}:"
        extendResult figure.genMoves(subMove.direction, subMove.cells.i, simulate)
      of CVariable:
        debug &"  SUB {subMove.direction} N"
        extendResult figure.genMoves(subMove.direction, 1 .. VARIABLE_N, simulate)
      of CRange:
        debug &"  SUB {subMove.direction} {subMove.cells.r}:"
        extendResult figure.genMoves(subMove.direction, subMove.cells.r, simulate)


proc possibleChecker(game: Game, kingLocation: Location): bool =
  for line in game.possibleAnswers:
    for cell in line:
      for answer in cell:
        if answer.target == kingLocation:
          return true
  return false


proc isLegal(figure: Figure, move: Move): bool =
  true


proc genPossibleMoves(game: Game, color: Color, simulate: bool)


proc makeMove(game: Game, move: Move) =
  game.moves[game.turn].add(move)
  let attacked = game.on(move.attack)
  if not attacked.isNil:
    if attacked.lives == 1:
      game.currentPiece.line = move.target.line
      game.currentPiece.column = move.target.column
      game.board[move.target.line][move.target.column] = game.currentPiece
      game.board[move.`from`.line][move.`from`.column] = nil
    attacked.lives -= 1
  else:
    game.board[move.target.line][move.target.column] = game.currentPiece
    game.currentPiece.line = move.target.line
    game.currentPiece.column = move.target.column
    game.board[move.`from`.line][move.`from`.column] = nil
  dumpBoard game
  game.genPossibleMoves(game.turn, true)
  for line in game.possibleAnswers:
    for cell in line:
      for answer in cell:
        if game.possibleChecker(game.kingLocations[~game.turn]):
          game.inCheckers[~game.turn] = true
          break
      if game.inCheckers[~game.turn]:
        break
    if game.inCheckers[~game.turn]:
      break


proc pseudoLegalMoves(figure: Figure, simulate: bool = false): seq[Move] =
  if figure.kind == "pawn":
    figure.possiblePawnMoves(simulate)
  else:
    figure.possibleRuleMoves(simulate)


proc genPossibleMoves(figure: Figure, simulate: bool) =
  let first = figure.pseudoLegalMoves(simulate)
  let moves = first.filterIt(figure.isLegal(it))
  var legalMoves: seq[Move] = @[]
  if not simulate:
    for move in moves:
      figure.game.genPossibleMoves(~figure.game.turn, true)
      if not figure.game.possibleChecker(figure.game.kingLocations[figure.color]):
        if not simulate:
          echo "no checker"
          figure.game.possibleMovesCount += 1
        legalMoves.add(move)

  if not simulate:
    figure.game.possibleMoves[figure.line][figure.column] = legalMoves
  else:
    figure.game.possibleAnswers[figure.line][figure.column] = moves


proc genPossibleMoves(game: Game, color: Color, simulate: bool) =
  if game.possibleMovesCached and not simulate:
    return
  
  for line in game.board:
    for cell in line:
      if not cell.isNil and cell.color == color:
        cell.genPossibleMoves(simulate)
  if not simulate:
    game.possibleMovesCached = true


proc finishGame*(game: Game, finish: Finish) =
  echo finish


proc onPiece*(game: Game, location: Location) =
  game.currentPiece = nil
  let figure = game.on(location)

  dumpBoard game
  if figure.isNil or figure.color != game.turn:
    dump location
    dump figure.isNil
    return

  game.currentPiece = figure


proc onFinishMove*(game: Game)


proc onDropPiece*(game: Game, location: Location) =
  echo "onDropPiece "
  dump game.currentPiece.isNil
  for move in game.possibleMoves[game.currentPiece.line][game.currentPiece.column]:
    if move.attack == location:
      game.makeMove(move)
      game.onFinishMove()
      return


proc onNewMove*(game: Game) =
  game.possibleMovesCached = false
  game.possibleMovesCount = 0
  game.genPossibleMoves(game.turn, false)
  if game.possibleMovesCount == 0:
    if game.inCheckers[game.turn]:
      game.finishGame(Finish.Checkmate)
    else:
      game.finishGame(Finish.Stalemate)


proc onFinishMove*(game: Game) =
  game.turn = ~game.turn


proc newGame*(ruleset: Ruleset): Game =
  result = Game(turn: Color.White, ruleset: ruleset)
  result.moves[Color.White] = @[]
  result.moves[Color.Black] = @[]
  result.names[Color.White] = ""
  result.names[Color.Black] = ""
  result.kingLocations[Color.White] = Location(line: 1, column: 5)
  result.kingLocations[Color.Black] = Location(line: 8, column: 5)
  for name, rule in ruleset.rules:
    for location in rule.start:
      for color in [Color.White, Color.Black]:
        echo location.line.relative(color), name
        result.board[location.line.relative(color)][location.column] = Figure(
          color: color,
          kind: name,
          line: location.line.relative(color),
          column: location.column,
          game: result,
          lives: rule.lives)
  



# gen all pseudoLegalMoves
# gen all pseudoLegalMoves for each one
# if any of them is chess bad

